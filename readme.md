<p align="center"><img src="https://img.shields.io/badge/Katsman%20-Table-blue?style=for-the-badge&logo=appveyor" height="50"></p>

<p><img src="https://img.shields.io/github/issues/AitzazImtiaz/Katsman-table?style=social&logo=appveyor"><img src="https://img.shields.io/github/forks/AitzazImtiaz/Katsman-table?style=social&logo=appveyor"> <img src="https://img.shields.io/github/stars/AitzazImtiaz/Katsman-table?style=social&logo=appveyor"><img src="https://img.shields.io/github/license/AitzazImtiaz/Katsman-table?style=social&logo=appveyor"> <img src="https://img.shields.io/twitter/url?url=https%3A%2F%2Fgithub.com%2FAitzazImtiaz%2FKatsman-table">

This is the implementation of Katsman Table, a 21st Century theory alternative to the Pascal Triangle. The program explains what is it, and help in common day Permutations, This program claims to determine 1-65 rows of this table appropriately, but C++ although strong, is in-sufficient to hold the data, and shows the accurate table rows 1-20, other numbers can be misleading since they cross billions atleast.

This code is collaboration between Aitzaz Imtiaz and GIA, and licensed under MIT.

Reprise! A better version of Katsman Table will be released! Only on Stev 3.
The pro-version is [here](https://github.com/AitzazImtiaz/Katsman-Table-Pro)
